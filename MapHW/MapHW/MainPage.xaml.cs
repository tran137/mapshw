﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;
using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace MapHW
{
	public partial class MainPage : ContentPage
	{
        List<Pin> pinList = new List<Pin>();
        int numPins = 5;

        public MainPage()
		{
			InitializeComponent();
            MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.13, -117.16), Distance.FromMiles(0.5)));
            InitPins();
            InitPicker();
        }

        void InitPicker()
        {
            var pickerList = new List<string>();

            for(int i = 0; i < numPins; i++)
            {
                pickerList.Add(pinList[i].Label);
            }
            locpicker.ItemsSource = pickerList;
        }

        void InitPins()
        {
            var pin1 = new Pin
            {
                Type = PinType.Place,
                Position = new Position(33.1353362, -117.1901207),
                Label = "Sublime Alehouse",
                Address = "1020 W San Marcos Blvd, San Marcos, CA 92078"
            };
            var pin2 = new Pin
            {
                Type = PinType.Place,
                Position = new Position(33.0711065, -117.0648411),
                Label = "Nordstrom Marketplace Cafe",
                Address = "3435 Del Mar Heights Rd D3, San Diego, CA 92130"
            };
            var pin3 = new Pin
            {
                Type = PinType.Place,
                Position = new Position(33.2024174, -117.2423293),
                Label = "Mother Earth Taphouse",
                Address = "206 Main St, Vista, CA 92084"
            };
            var pin4 = new Pin
            {
                Type = PinType.Place,
                Position = new Position(32.9530758, -117.2325918),
                Label = "Snooze an A.M. Eatery",
                Address = "3435 Del Mar Heights Rd D3, San Diego, CA 92130"
            };
            var pin5 = new Pin
            {
                Type = PinType.Place,
                Position = new Position(32.9573279, -117.1252683),
                Label = "Sab-E-Lee",
                Address = "13223 Black Mountain Rd, San Diego, CA 92129"
            };

            pinList.Add(pin1);
            pinList.Add(pin2);
            pinList.Add(pin3);
            pinList.Add(pin4);
            pinList.Add(pin5);

            MyMap.Pins.Add(pin1);
            MyMap.Pins.Add(pin2);
            MyMap.Pins.Add(pin3);
            MyMap.Pins.Add(pin4);
            MyMap.Pins.Add(pin5);
        }

        void OnClickStreet(object sender, EventArgs e)
        {
            MyMap.MapType = MapType.Street;
        }

        void OnClickHybrid(object sender, EventArgs e)
        {
            MyMap.MapType = MapType.Hybrid;
        }

        void OnClickSatellite(object sender, EventArgs e)
        {
            MyMap.MapType = MapType.Satellite;
        }

        void OnPickerIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            int selectedIndex = picker.SelectedIndex;

            if (selectedIndex != -1)
            {
                MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(pinList[selectedIndex].Position, Distance.FromMiles(0.5)));
            }
        }
    }
}
